<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.min.js","","",1);
    wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.min.js","","",1);    
});




//FacetWP includes built-in accessibility (a11y) support but it’s disabled by default. To enable it
add_filter( 'facetwp_load_a11y', '__return_true' );

// Register menus
function register_my_menus() {
    register_nav_menus(
        array(
            'footer-1' => __( 'Footer Menu 1' ),
            'footer-2' => __( 'Footer Menu 2' ),
            'footer-3' => __( 'Footer Menu 3' ),
            'footer-4' => __( 'Footer Menu 4' ),
            'footer-5' => __( 'Footer Menu 5' ),
            'site-map' => __( 'Site Map' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );


 
// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');


function register_shortcodes(){
  $dir=dirname(__FILE__)."/shortcodes";
  $files=scandir($dir);
  foreach($files as $k=>$v){
    $file=$dir."/".$v;
    if(strstr($file,".php")){
      $shortcode=substr($v,0,-4);
      add_shortcode($shortcode,function($attr,$content,$tag){
        ob_start();
        include(dirname(__FILE__)."/shortcodes/".$tag.".php");
        $c=ob_get_clean();
        return $c;
      });
    }
  }
}



// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');





function fr_img($id=0,$size="",$url=false,$attr=""){

    //Show a theme image
    if(!is_numeric($id) && is_string($id)){
        $img=get_stylesheet_directory_uri()."/images/".$id;
        if(file_exists(to_path($img))){
            if($url){
                return $img;
            }
            return '<img src="'.$img.'" '.($attr?build_attr($attr):"").'>';
        }
    }

    //If ID is empty get the current post attachment id
    if(!$id){
        $id=get_post_thumbnail_id();
    }

    //If Id is object it means that is a post object, thus retrive the post ID
    if(is_object($id)){
        if(!empty($id->ID)){
            $id=$id->ID;
        }
    }

    //If ID is not an attachment than get the attachment from that post
    if(get_post_type($id)!="attachment"){
        $id=get_post_thumbnail_id($id);
    }

    if($id){
        $image_url=wp_get_attachment_image_url($id,$size);
        if(!$url){
            //If image is a SVG embed the contents so we can change the color dinamically
            if(substr($image_url,-4,4)==".svg"){
                $image_url=str_replace(get_bloginfo("url"),ABSPATH."/",$image_url);
                $data=file_get_contents($image_url);
                echo strstr($data,"<svg ");
            }else{
                return wp_get_attachment_image($id,$size,0,$attr);
            }
        }else if($url){
            return $image_url;
        }
    }
}

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );


function featured_products_fun() {
	$args = array(
        'post_type' => array('carpeting', 'luxury_vinyl_tile', 'solid_wpc_waterproof', 'hardwood_catalog', 'laminate_catalog', 'tile_catalog'),
        'post__not_in' => array(323965,302354), 
        'posts_per_page' => 20,
        'orderby' => 'rand',
        'meta_query' => array(
            array(
                'key' => 'featured', 
                'value' => true, 
                'compare' => '=='
            )
        ) 
    );
    $query = new WP_Query( $args );
    if( $query->have_posts() ){
        $outpout = '<div class="featured-products"><div class="featured-product-list">';
        while ($query->have_posts()) : $query->the_post(); 
            // $gallery_images = get_field('gallery_room_images');
            // $gallery_img = explode("|",$gallery_images);
            // $count = 0;
            // // loop through the rows of data
            // foreach($gallery_img as  $key=>$value) {
            //     $room_image = $value;
            //     if(!$room_image){
            //         $room_image = "http://placehold.it/245x310?text=No+Image";
            //     }
            //     else{
            //         if(strpos($room_image , 's7.shawimg.com') !== false){
            //             if(strpos($room_image , 'http') === false){ 
            //                 $room_image = "http://" . $room_image;
            //             }
            //             $room_image = $room_image ;
            //         } else{
            //             if(strpos($room_image , 'http') === false){ 
            //                 $room_image = "https://" . $room_image;
            //             }
            //             $room_image= "https://mobilem.liquifire.com/mobilem?source=url[".$room_image . "]&scale=size[524x636]&sink";
            //         }
            //     }
            //     if($count>0){
            //         break;
            //     }
            //     $count++;
            // }

            $image = get_field('swatch_image_link') ? get_field('swatch_image_link'):"http://placehold.it/168x123?text=No+Image"; 

            if(strpos($image , 's7.shawimg.com') !== false){
                if(strpos($image , 'http') === false){ 
                    $image = "http://" . $image;
                }	
                $image_thumb = "https://mobilem.liquifire.com/mobilem?source=url[".$image . "]&scale=size[150]&sink";
                $image = "https://mobilem.liquifire.com/mobilem?source=url[".$image . "]&scale=size[300x300]&sink";
            }
            else{
                if(strpos($image , 'http') === false){ 
                    $image = "https://" . $image;
                }	
                $image_thumb = "https://mobilem.liquifire.com/mobilem?source=url[".$image . "]&scale=size[150]&sink";
                $image = "https://mobilem.liquifire.com/mobilem?source=url[".$image . "]&scale=size[600x400]&sink";
            }

            $color_name = get_field('color');
            $post_type = get_post_type_object(get_post_type( get_the_ID() ));


            $brand = get_field('brand');
            $collection = get_field('collection');

            $productName = $brand.' '.$collection;

            $in_stock = get_field('in_stock', get_the_ID());     
            if($in_stock==1){
                $in_stock_txt = '<span class="stock-txt">Best Value</span>';
            }
            else{
                $in_stock_txt = '';
            }

            $familysku = get_post_meta(get_the_ID(), 'collection', true);
            $args1 = array(
                'post_type'      => array('carpeting', 'luxury_vinyl_tile', 'solid_wpc_waterproof', 'hardwood_catalog', 'laminate_catalog', 'tile_catalog'),
                'posts_per_page' => -1,
                'post_status'    => 'publish',
                'meta_query'     => array(
                    array(
                        'key'     => 'collection',
                        'value'   => $familysku,
                        'compare' => '='
                    )
                )
            );
            $the_query = new WP_Query( $args1 );

            
            $outpout .= '<div class="featured-product-item">
                <div class="featured-inner">
                    <div class="prod-img-wrapper">
                        '.$in_stock_txt.'
                        <a href="'.get_the_permalink().'" class="prod-img-wrap"><img src="'.$image.'" alt="'.get_the_title().'" /></a>
                    </div>
                    <div class="product-info">
                        <h2><a href="'.get_the_permalink().'">'.$color_name.'</a></h2>
                        <h5>'.$brand.'</h5>
                        <h4>'.$the_query->found_posts.' Colors</h4>
                    </div>
                </div>
            </div>';

        endwhile;
        $outpout .= '</div></div>';
        wp_reset_query();
    }  


    return $outpout;
}
add_shortcode( 'featured_products', 'featured_products_fun' );

//Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter( 'wpseo_breadcrumb_links', 'wpse_override_yoast_breadcrumb_trail',90 );

function wpse_override_yoast_breadcrumb_trail( $links ) {

    if (is_singular( 'carpeting' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/carpet/',
            'text' => 'Carpet',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/browse-carpet/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }

    if (is_singular( 'hardwood_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/hardwood/',
            'text' => 'Hardwood Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/browse-hardwood/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }

    if (is_singular( 'laminate_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/laminate/',
            'text' => 'Laminate Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/browse-laminate/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }

    if (is_singular( 'luxury_vinyl_tile' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/luxury-vinyl/',
            'text' => 'Luxury Vinyl',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/browse-luxury-vinyl/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }
    if (is_singular( 'tile_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/tile/',
            'text' => 'Tile',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/browse-ceramic/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }
    
    return $links;
}



function featured_products_slider_fun($atts) {

    // print_r($atts);
 
     $args = array(
         'post_type' => $atts['0'],        
         'posts_per_page' => 8,
         'orderby' => 'rand',
         'meta_query' => array(
             array(
                 'key' => 'featured', 
                 'value' => '1', 
                 'compare' => '=='
             )
         ) 
     );
     $query = new WP_Query( $args );
     if( $query->have_posts() ){
         $outpout = '<div class="featured-products"><div class="featured-product-list">';
         while ($query->have_posts()) : $query->the_post(); 
             $gallery_images = get_field('gallery_room_images');
             $gallery_img = explode("|",$gallery_images);
            
             // loop through the rows of data

             $room_image =  single_gallery_image_product(get_the_ID(),'600','400');
            
 
             $color_name = get_field('color');
             $brand_name = get_field('brand');
             $collection = get_field('collection');
             $post_type = get_post_type_object(get_post_type( get_the_ID() ));
            
             
             $outpout .= '<div class="featured-product-item">
                 <div class="featured-inner">
                     <div class="prod-img-wrap">
                         <img src="'.$room_image.'" alt="'.get_the_title().'" />
                         <div class="button-wrapper">
                             <div class="button-wrapper-inner">
                                 <a href="'.get_the_permalink().'" class="button alt viewproduct-btn">View Product</a>';
              
            if( get_option('getcouponbtn') == 1){

                $outpout .= '<a href="/flooring-coupon/" class="button fea-coupon_btn">Get Coupon</a>';
            }
              
              $outpout .= '</div>
                         </div>
                     </div>
                     <div class="product-info">
                         <div class="slider-product-title"><a href="'.get_the_permalink().'">'.$collection.'</a></div>
                         <h3 class="floorte-color">'.$color_name.'</h3>
                     </div>
                 </div>
             </div>';
 
         endwhile;
         $outpout .= '</div></div>';
         wp_reset_query();
     }  
 
 
     return $outpout;
 }
 add_shortcode( 'featured_products_slide', 'featured_products_slider_fun' );

 add_filter('fl_builder_module_attributes', function ($attrs, $row) {
    if ('test111' == $row->settings->id) {
      $attrs['data-foo'] = 'bar';
    }
    return $attrs;
  }, 10, 2);

  add_filter('fl_builder_row_attributes', function ($attrs, $row) {
    if ('test341' == $row->settings->id) {
      $attrs['data-foo'] = 'bar';
    }
    return $attrs;
  }, 10, 2);


  
//  IP location FUnctionality
//   if (!wp_next_scheduled('cde_preferred_location_cronjob')) {
    
//     wp_schedule_event( time() +  17800, 'daily', 'cde_preferred_location_cronjob');
// }

add_action( 'cde_preferred_location_cronjob', 'cde_preferred_location' );

function cde_preferred_location(){

          global $wpdb;

          if ( ! function_exists( 'post_exists' ) ) {
              require_once( ABSPATH . 'wp-admin/includes/post.php' );
          }

          //CALL Authentication API:
          $apiObj = new APICaller;
          $inputs = array('grant_type'=>'client_credentials','client_id'=>get_option('CLIENT_CODE'),'client_secret'=>get_option('CLIENTSECRET'));
          $result = $apiObj->call(AUTHURL,"POST",$inputs,array(),AUTH_BASE_URL);


          if(isset($result['error'])){
              $msg =$result['error'];                
              $_SESSION['error'] = $msg;
              $_SESSION["error_desc"] =$result['error_description'];
              
          }
          else if(isset($result['access_token'])){

              //API Call for getting website INFO
              $inputs = array();
              $headers = array('authorization'=>"bearer ".$result['access_token']);
              $website = $apiObj->call(BASEURL.get_option('SITE_CODE'),"GET",$inputs,$headers);

              write_log($website['result']['locations']);

              for($i=0;$i<count($website['result']['locations']);$i++){

                  if($website['result']['locations'][$i]['type'] == 'store'){

                      $location_name = isset($website['result']['locations'][$i]['city'])?$website['result']['locations'][$i]['city']:"";

                      $found_post = post_exists($location_name,'','','store-locations');

                          if( $found_post == 0 ){

                                  $array = array(
                                      'post_title' => $location_name,
                                      'post_type' => 'store-locations',
                                      'post_content'  => "",
                                      'post_status'   => 'publish',
                                      'post_author'   => 0,
                                  );
                                  $post_id = wp_insert_post( $array );

                                  write_log( $location_name.'---'.$post_id);
                                  
                                  update_post_meta($post_id, 'address', $website['result']['locations'][$i]['address']); 
                                  update_post_meta($post_id, 'city', $website['result']['locations'][$i]['city']); 
                                  update_post_meta($post_id, 'state', $website['result']['locations'][$i]['state']); 
                                  update_post_meta($post_id, 'country', $website['result']['locations'][$i]['country']); 
                                  update_post_meta($post_id, 'postal_code', $website['result']['locations'][$i]['postalCode']); 
                                  if($website['result']['locations'][$i]['forwardingPhone']==''){

                                    update_post_meta($post_id, 'phone', $website['result']['locations'][$i]['phone']);  
                                    
                                  }else{

                                    update_post_meta($post_id, 'phone', $website['result']['locations'][$i]['forwardingPhone']);  
                                  }
                                                                     
                                  update_post_meta($post_id, 'latitude', $website['result']['locations'][$i]['lat']); 
                                  update_post_meta($post_id, 'longitue', $website['result']['locations'][$i]['lng']); 
                                  update_post_meta($post_id, 'monday', $website['result']['locations'][$i]['monday']); 
                                  update_post_meta($post_id, 'tuesday', $website['result']['locations'][$i]['tuesday']); 
                                  update_post_meta($post_id, 'wednesday', $website['result']['locations'][$i]['wednesday']); 
                                  update_post_meta($post_id, 'thursday', $website['result']['locations'][$i]['thursday']); 
                                  update_post_meta($post_id, 'friday', $website['result']['locations'][$i]['friday']); 
                                  update_post_meta($post_id, 'saturday', $website['result']['locations'][$i]['saturday']); 
                                  update_post_meta($post_id, 'sunday', $website['result']['locations'][$i]['sunday']); 
                                  
                          }else{

                                    update_post_meta($found_post, 'address', $website['result']['locations'][$i]['address']); 
                                    update_post_meta($found_post, 'city', $website['result']['locations'][$i]['city']); 
                                    update_post_meta($found_post, 'state', $website['result']['locations'][$i]['state']); 
                                    update_post_meta($found_post, 'country', $website['result']['locations'][$i]['country']); 
                                    update_post_meta($found_post, 'postal_code', $website['result']['locations'][$i]['postalCode']); 
                                    if($website['result']['locations'][$i]['forwardingPhone']==''){

                                    update_post_meta($found_post, 'phone', $website['result']['locations'][$i]['phone']);  
                                    
                                    }else{

                                    update_post_meta($found_post, 'phone', $website['result']['locations'][$i]['forwardingPhone']);  
                                    }
                                                                    
                                    update_post_meta($found_post, 'latitude', $website['result']['locations'][$i]['lat']); 
                                    update_post_meta($found_post, 'longitue', $website['result']['locations'][$i]['lng']); 
                                    update_post_meta($found_post, 'monday', $website['result']['locations'][$i]['monday']); 
                                    update_post_meta($found_post, 'tuesday', $website['result']['locations'][$i]['tuesday']); 
                                    update_post_meta($found_post, 'wednesday', $website['result']['locations'][$i]['wednesday']); 
                                    update_post_meta($found_post, 'thursday', $website['result']['locations'][$i]['thursday']); 
                                    update_post_meta($found_post, 'friday', $website['result']['locations'][$i]['friday']); 
                                    update_post_meta($found_post, 'saturday', $website['result']['locations'][$i]['saturday']); 
                                    update_post_meta($found_post, 'sunday', $website['result']['locations'][$i]['sunday']); 

                          }

                  }
              
              }

          }    

}



//get ipaddress of visitor

function getUserIpAddr() {
  $ipaddress = '';
  if (isset($_SERVER['HTTP_CLIENT_IP']))
      $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
  else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
      $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
  else if(isset($_SERVER['HTTP_X_FORWARDED']))
      $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
  else if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
      $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
  else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
      $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
  else if(isset($_SERVER['HTTP_FORWARDED']))
      $ipaddress = $_SERVER['HTTP_FORWARDED'];
  else if(isset($_SERVER['REMOTE_ADDR']))
      $ipaddress = $_SERVER['REMOTE_ADDR'];
  else
      $ipaddress = 'UNKNOWN';
  
  if ( strstr($ipaddress, ',') ) {
          $tmp = explode(',', $ipaddress,2);
          $ipaddress = trim($tmp[1]);
  }
  return $ipaddress;
}


// Custom function for lat and long

function get_storelisting() {
  
  global $wpdb;
  $content="";


//  echo 'no cookie';
  
  $urllog = 'https://global-ds.cloud.netacuity.com/webservice/query';
  //12.68.65.195
  $response = wp_remote_post( $urllog, array(
      'method' => 'GET',
      'timeout' => 45,
      'redirection' => 5,
      'httpversion' => '1.0',
      'headers' => array('Content-Type' => 'application/json'),         
      'body' =>  array('u'=> 'b25d7667-74cc-4fcc-9adf-7b5f4f8f5bd0','ip'=> getUserIpAddr(),'dbs'=> 'all','trans_id'=> 'example','json'=> 'true' ),
      'blocking' => true,               
      'cookies' => array()
      )
  );
      
      $rawdata = json_decode($response['body'], true);
      $userdata = $rawdata['response'];

      $autolat = $userdata['pulseplus-latitude'];
      $autolng = $userdata['pulseplus-longitude'];
      if(isset($_COOKIE['preferred_store'])){

          $store_location = $_COOKIE['preferred_store'];
      }else{

          $store_location = '';
         
        }

      //print_r($rawdata);
      
      $sql =  "SELECT post_lat.meta_value AS lat,post_lng.meta_value AS lng,posts.ID, 
              ( 3959 * acos( cos( radians(".$autolat." ) ) * cos( radians( post_lat.meta_value ) ) * 
              cos( radians( post_lng.meta_value ) - radians( ".$autolng." ) ) + sin( radians( ".$autolat." ) ) * 
              sin( radians( post_lat.meta_value ) ) ) ) AS distance FROM wp_posts AS posts
              INNER JOIN wp_postmeta AS post_lat ON post_lat.post_id = posts.ID AND post_lat.meta_key = 'latitude'
              INNER JOIN wp_postmeta AS post_lng ON post_lng.post_id = posts.ID AND post_lng.meta_key = 'longitue'  
              WHERE posts.post_type = 'store-locations' 
              AND posts.post_status = 'publish' GROUP BY posts.ID HAVING distance < 50000000000000000000 ORDER BY distance";

              write_log($sql);

      $storeposts = $wpdb->get_results($sql);
      $storeposts_array = json_decode(json_encode($storeposts), true);      

     
      if($store_location ==''){
          //write_log('empty loc');           
          $store_location = $storeposts_array['0']['ID'];
          $store_distance = round($storeposts_array['0']['distance'],1);
      }else{

         // write_log('noempty loc');
          $key = array_search($store_location, array_column($storeposts_array, 'ID'));
          $store_distance = round($storeposts_array[$key]['distance'],1);           

      }
      
    
      $content = get_the_title($store_location).', '.get_field('state', $store_location) ; 
      $format_phone = formatPhoneNumber(get_field('phone', $store_location)); 
      $text_phone = get_field('phone', $store_location); 

  foreach ( $storeposts as $post ) {

    write_log(get_field('phone', $post->ID));
   
                $content_list .= '<div class="location-section" id ="'.$post->ID.'">
                <div class="location-info-section">
                    <h2 class="location-name">'.get_the_title($post->ID).' - <b>'.round($post->distance,1).' MI</b></h2>
                    <p class="store-add"> '.get_field('address', $post->ID).'<br>'.get_field('city',$post->ID).', '.get_field('state',$post->ID).' '.get_field('postal_code', $post->ID).'</p>
                    <p class="store-phone"><i class="fa fa-phone-alt"></i> &nbsp;'.get_field('phone', $post->ID).'</p>
                    <div class="store-hour-section">
                        <div class="label">STORE HOURS</div>
                        <ul class="storename"><li><div class="store-opening-hrs-container">
                        <ul class="store-opening-hrs">
                            <li>Monday: <span>'.get_field('monday', $post->ID).'</span></li>
                            <li>Tuesday: <span>'.get_field('tuesday', $post->ID).'</span></li>
                            <li>Wednesday: <span>'.get_field('wednesday', $post->ID).'</span></li>
                            <li>Thursday: <span>'.get_field('thursday', $post->ID).'</span></li>
                            <li>Friday: <span>'.get_field('friday', $post->ID).'</span></li>
                            <li>Saturday: <span>'.get_field('saturday', $post->ID).'</span></li>
                            <li>Sunday: <span>'.get_field('sunday', $post->ID).'</span></li>
                        </ul>
                        </div></li></ul>
                    </div>
                    '.do_shortcode('[storelocation_address "dir" "'.get_the_title($post->ID).'"]').'
                    <a href="'.get_field('store_link', $post->ID).'"  class="fl-button-link" role="button"> View Location</a>
                    <a href="javascript:void(0)" data-id="'.$post->ID.'" data-storename="'.get_the_title($post->ID).'" data-fphone="'.formatPhoneNumber(get_field('phone',$post->ID)).'" data-phone="'.get_field('phone',$post->ID).'" data-distance="'.round($post->distance,1).'" target="_self" role="button" class="fl-button choose_location">Choose This Location</a>
                </div>
            </div>'; 
  } 
  

  $data = array();

  $data['header']= $content;
  $data['formatphone']= $format_phone;
  $data['text_phone']= $text_phone;
  $data['list']= $content_list;

  //write_log($data['list']);

  echo json_encode($data);
      wp_die();
}

//add_shortcode('storelisting', 'get_storelisting');
add_action( 'wp_ajax_nopriv_get_storelisting', 'get_storelisting' );
add_action( 'wp_ajax_get_storelisting', 'get_storelisting' );



//choose this location FUnctionality

add_action( 'wp_ajax_nopriv_choose_location', 'choose_location' );
add_action( 'wp_ajax_choose_location', 'choose_location' );

function choose_location() {     
      
      $content = get_the_title($_POST['store_id']).', '.get_field('state', $_POST['store_id']) ; 
      $format_phone = formatPhoneNumber(get_field('phone', $_POST['store_id'])); 
      $text_phone = get_field('phone', $_POST['store_id']); 

        $data = array();

        $data['header_location']= $content;
        $data['formatphone']= $format_phone;
        $data['text_phone']= $text_phone;  

    echo json_encode($data);
  wp_die();
}

//   function my_after_header() {
//       echo '<div class="header_store"></div><div id="ajaxstorelisting"> </div>
//       <div class="locationOverlay"></div>';
//     }
//     add_action( 'fl_after_header', 'my_after_header' );

function location_header() {
    return '<div class="header_store"></div><div id="ajaxstorelisting"> </div>
    <div class="locationOverlay"></div>';
  }
add_shortcode('location_code', 'location_header');

add_filter( 'gform_pre_render_18', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_18', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_18', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_18', 'populate_product_location_form' );

// add_filter( 'gform_pre_render_19', 'populate_product_location_form' );
// add_filter( 'gform_pre_validation_19', 'populate_product_location_form' );
// add_filter( 'gform_pre_submission_filter_19', 'populate_product_location_form' );
// add_filter( 'gform_admin_pre_render_19', 'populate_product_location_form' );

// add_filter( 'gform_pre_render_18', 'populate_product_location_form' );
// add_filter( 'gform_pre_validation_18', 'populate_product_location_form' );
// add_filter( 'gform_pre_submission_filter_18', 'populate_product_location_form' );
// add_filter( 'gform_admin_pre_render_18', 'populate_product_location_form' );

// add_filter( 'gform_pre_render_33', 'populate_product_location_form' );
// add_filter( 'gform_pre_validation_33', 'populate_product_location_form' );
// add_filter( 'gform_pre_submission_filter_33', 'populate_product_location_form' );
// add_filter( 'gform_admin_pre_render_33', 'populate_product_location_form' );

// add_filter( 'gform_pre_render_34', 'populate_product_location_form' );
// add_filter( 'gform_pre_validation_34', 'populate_product_location_form' );
// add_filter( 'gform_pre_submission_filter_34', 'populate_product_location_form' );
// add_filter( 'gform_admin_pre_render_34', 'populate_product_location_form' );

// add_filter( 'gform_pre_render_35', 'populate_product_location_form' );
// add_filter( 'gform_pre_validation_35', 'populate_product_location_form' );
// add_filter( 'gform_pre_submission_filter_35', 'populate_product_location_form' );
// add_filter( 'gform_admin_pre_render_35', 'populate_product_location_form' );

function populate_product_location_form( $form ) {

foreach ( $form['fields'] as &$field ) {

    // Only populate field ID 12
    if ( $field->type != 'select' || strpos( $field->cssClass, 'populate-store' ) === false ) {
        continue;
    }      	

        $args = array(
            'post_type'      => 'store-locations',
            'posts_per_page' => -1,
            'post_status'    => 'publish'
        );										
      
         $locations =  get_posts( $args );

         $choices = array(); 

         foreach($locations as $location) {
            

              $title = get_the_title($location->ID);
              
                   $choices[] = array( 'text' => $title, 'value' => $title );

          }
          wp_reset_postdata();

         // write_log($choices);

         // Set placeholder text for dropdown
         $field->placeholder = '-- Choose Location --';

         // Set choices from array of ACF values
         $field->choices = $choices;
    
}
return $form;

}