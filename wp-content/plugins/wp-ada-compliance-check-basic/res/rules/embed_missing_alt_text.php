<?php
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
/********************************************************************/	
// missing alt text on embed tags
/********************************************************************/	
function wp_ada_compliance_basic_validate_embed_missing_alt_text($content, $postinfo){
	
global $wp_ada_compliance_basic_def;
	
$dom = str_get_html($content);

// get options
$wp_ada_compliance_basic_scanoptions = get_option('wp_ada_compliance_basic_ignore_scan_rules',array());
	
// check if being scanned
if(in_array('embed_missing_alt_text', $wp_ada_compliance_basic_scanoptions)) return 1;		

$embeds = $dom->find('embed');
foreach ($embeds as $embed) {
			
	if (isset($embed) and $embed->getAttribute('title') == "" and $embed->getAttribute('aria-label') == "") {  
		$embedcode = $embed->outertext;
							
			// save error
			if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"embed_missing_alt_text", $embedcode)){
			$insertid = wp_ada_compliance_basic_insert_error($postinfo,"embed_missing_alt_text", $wp_ada_compliance_basic_def['embed_missing_alt_text']['StoredError'],  $embedcode);
			}
			
			
		}
}
	return 1;
} 	
?>