<?php 
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
/********************************************************************/	
// validate audio or video players from remote sources
/********************************************************************/	
function wp_ada_compliance_basic_validate_video_player_remote_source($content, $postinfo){
	
global $wp_ada_compliance_basic_def;
	
$dom = str_get_html($content);

// get options
$wp_ada_compliance_basic_scanoptions = get_option('wp_ada_compliance_basic_ignore_scan_rules', array());
	
// check if being scanned
if(in_array('video_player_remote_source', $wp_ada_compliance_basic_scanoptions)) return 1;		
	
	
// check iframe tags
$iframes = $dom->find('iframe,embed');
foreach ($iframes as $iframe) {
		$videocode = $iframe->outertext;
		if (isset($iframe) and (
			
			// service providers
			strstr($videocode,'www.youtube.com') 
			or strstr($videocode,'vimeo.com')
		   or strstr($videocode,'videopress.com')
			or strstr($videocode,'embed.ted.com')
			or strstr($videocode,'hulu.com')
			or strstr($videocode,'wordpress.tv')
			or strstr($videocode,'animoto.com')
			or strstr($videocode,'blip.com')
			or strstr($videocode,'vine.com')
			or strstr($videocode,'collegehumor.com')
			or strstr($videocode,'dailymotion.com')
			or strstr($videocode,'funnyordie.com')
			or strstr($videocode,'mixcloud.com')
			or strstr($videocode,'reverbnation.com')
			or strstr($videocode,'soundcloud.com')
			or strstr($videocode,'spotify.com')
		   )){			
			// save error
			if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"video_player_remote_source", $videocode))
			$insertid = wp_ada_compliance_basic_insert_error($postinfo,"video_player_remote_source",$wp_ada_compliance_basic_def['video_player_remote_source']['StoredError'], $videocode);
			
		}
}
		
return 1;
}
?>