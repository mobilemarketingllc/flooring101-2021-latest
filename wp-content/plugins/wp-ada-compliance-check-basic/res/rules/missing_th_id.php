<?php
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
/********************************************************************/	
// validate tables missing th id	
/********************************************************************/	
function wp_ada_compliance_basic_validate_missing_th_id($content, $postinfo){
	
global $wp_ada_compliance_basic_def;
		
$dom = str_get_html($content);

// get options
$wp_ada_compliance_basic_scanoptions = get_option('wp_ada_compliance_basic_ignore_scan_rules',array());
	
// check if being scanned
if(in_array('missing_th_id', $wp_ada_compliance_basic_scanoptions)) return 1;	

$tables = $dom->find('table');
	$k = 0;
foreach ($tables as $table) {	

$tablecode = $table->outertext;

if($table != "" and stristr($tablecode, "<th")){	


$tablecells = $table->find('td');
		$additionalerrorInfo =  __("Missing ID: ","wp-ada-compliance-basic");
	foreach ($tablecells as $td) {
		
      if (isset($td) and $td->getAttribute('headers') != ""){
		$headerids = explode(" ",$td->getAttribute('headers'));
		foreach($headerids as $headeridvalue){
			if(!stristr($tablecode, 'id="'.$headeridvalue.'"') and !stristr($tablecode, "id='".$headeridvalue."'")) {	
			//if(!preg_match('#<th(\s|\S)*(id=)+(("|\')'.$headeridvalue.')+#', $tablecode)) {		
			if(!strstr($additionalerrorInfo, $headeridvalue)) $additionalerrorInfo .= $headeridvalue." ";
			}
			} // end headerid foreach
	  		}// end td header check
			} // end td foreach
			if(isset($additionalerrorInfo) and $additionalerrorInfo !=  __("Missing ID: ","wp-ada-compliance-basic")){
			$additionalerrorInfo .= $tablecode;	  
			// save error
			if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"missing_th_id", $additionalerrorInfo)){		
			$insertid = wp_ada_compliance_basic_insert_error($postinfo,"missing_th_id", $wp_ada_compliance_basic_def['missing_th_id']['StoredError'],  $additionalerrorInfo);	
			}
				 
		}
	
	}

}
return 1;
}

?>