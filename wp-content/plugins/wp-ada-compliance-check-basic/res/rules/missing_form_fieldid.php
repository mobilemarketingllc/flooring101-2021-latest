<?php
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
/********************************************************************/	
//look for form labels without matching form field ids
/********************************************************************/	
function wp_ada_compliance_basic_validate_missing_form_fieldid($content, $postinfo){
	
global $wp_ada_compliance_basic_def;
	
$dom = str_get_html($content);		

// get options
$wp_ada_compliance_basic_scanoptions = get_option('wp_ada_compliance_basic_ignore_scan_rules',array());
	
// check if being scanned
if(in_array('missing_form_fieldid', $wp_ada_compliance_basic_scanoptions)) return 1;	
	
	    
// find labels with for attributes that don't match a field id 
$fields = $dom->find('input,select,textarea');
$fieldids = array(); 	
foreach ($fields as $field) {
if(isset($field)) $fieldids[] = $field->getAttribute('id');
}
if(!isset($fieldids)) $fieldids[] = "%^&*";
  
$labels = $dom->find('label');
foreach ($labels as $label) {
 if(!in_array($label->getAttribute('for'),$fieldids) and !stristr($label->parent()->getAttribute('style'),'display:none')){
    $missing_form_fieldid_errorcode = $label->outertext;
		
	// if not hidden from screen readers
	if(!stristr($missing_form_fieldid_errorcode,'tabindex="-1"') and !stristr($missing_form_fieldid_errorcode,"tabindex='-1'")){    
    		// save error
		if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"missing_form_fieldid", $missing_form_fieldid_errorcode)){
			
		$insertid = wp_ada_compliance_basic_insert_error($postinfo, "missing_form_fieldid", $wp_ada_compliance_basic_def['missing_form_fieldid']['StoredError'], $missing_form_fieldid_errorcode);
		}
		
		
	}
	
	
}
}
	return 1;
}
?>