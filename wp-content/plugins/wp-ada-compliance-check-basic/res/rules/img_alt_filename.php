<?php
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
/********************************************************************/	
// validate alt text content contains file name
/********************************************************************/
function wp_ada_compliance_basic_validate_img_alt_filename($content, $postinfo){
	
global $wp_ada_compliance_basic_def;
	
$dom = str_get_html($content);	

// get options
$wp_ada_compliance_basic_scanoptions = get_option('wp_ada_compliance_basic_ignore_scan_rules',array());
	
// check if being scanned
if(in_array('img_alt_filename', $wp_ada_compliance_basic_scanoptions)) return 1;		

$images = $dom->find('img');
foreach ($images as $image) {
        if (isset($image) and stristr($image->getAttribute('alt'),__('.jpg','wp-ada-compliance-basic')) 
			or stristr($image->getAttribute('alt'),__('.png','wp-ada-compliance-basic')) 
			or stristr($image->getAttribute('alt'),__('.gif','wp-ada-compliance-basic'))  
			or stristr($image->getAttribute('alt'),__('_','wp-ada-compliance-basic'))
            or stristr($image->getAttribute('alt'),__('DSCN','wp-ada-compliance-basic')) 
			or preg_match('/^\d[a-zA-Z]\d[a-zA-Z][\w\-_]+\d$/',$image->getAttribute('alt'))
			or preg_match('/^[a-zA-Z]+\d$/',$image->getAttribute('alt'))
		    ) {
			
			
				$imagecode = $image->outertext;
			
		 
			// save error
			if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"img_alt_filename", $imagecode))
			$insertid = wp_ada_compliance_basic_insert_error($postinfo,"img_alt_filename", $wp_ada_compliance_basic_def['img_alt_filename']['StoredError'], $imagecode);
			

				
		}
}
	return 1;
}
?>