<?php 
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
/********************************************************************/	
// adjacent link with the same destination but different link text
/********************************************************************/	
function wp_ada_compliance_basic_validate_inconsitent_link_text($content, $postinfo){
	
global $wp_ada_compliance_basic_def;
	
$dom = str_get_html($content);	

// get options
$wp_ada_compliance_basic_scanoptions = get_option('wp_ada_compliance_basic_ignore_scan_rules',array());
	

// check if being scanned
if(in_array('inconsitent_link_text', $wp_ada_compliance_basic_scanoptions)) return 1;
    
        // define site url	
$siteurl = esc_url_raw(get_site_url());
$site_url_patterns = array();
$site_url_patterns[] =  '|'.$siteurl."|i";
$site_url_patterns[] = '|'.preg_replace('#^[^\/]*#', '', preg_replace('#((https://)|http://)*#i','',$siteurl))."|i";	

$links = $dom->find('a');
	$count = 1;
    $linktext = array();
    $linkcode = array();
    $linkdestination = array();
	foreach ($links as $link) {
                  $arialabelledby = wp_ada_complaince_basic_get_aria_values($dom, $link, 'aria-labelledby');
      $ariadescribedby = wp_ada_complaince_basic_get_aria_values($dom, $link, 'aria-describedby');
        $img = $link->find('img');
                
        // aria label on lnks
       if($link->getAttribute('aria-label') != ""){
		$linktext[$count] = $link->getAttribute('aria-label');	
		}
             // aria - described by
        elseif($ariadescribedby != ""){
           $linktext[$count] =  $ariadescribedby;
        }
        // aria - labelled by 
        elseif( $arialabelledby != ""){
           $linktext[$count] =   $arialabelledby;
        }
        // images
        elseif(isset($img[0])) {
            if($img[0]->getAttribute('alt') != ''){
            $linktext[$count] = $img[0]->getAttribute('alt');
        }
        }      
        // title and link text
        elseif($link->getAttribute('title') != ""){ 
		$linktext[$count] = $link->plaintext.' '.$link->getAttribute('title');	
		}  
        // just link text
		else{
		$linktext[$count] = $link->plaintext;	
		}
		
        $linkcode[$count] = $link->outertext;
        $linkdestination[$count] = preg_replace($site_url_patterns, '',$link->getAttribute('href'));
        
        $excludedlinks[] = '';
        $excludedlinks[] = 'x';
        $excludedlinks[] = __('close','wp-ada-compliance-basic');
        
            $matchfound = 0;
            foreach($linktext as $key => $value){
               if(array_key_exists($key-1,$linkcode) 
                   and trim(strtolower($linktext[$key])) != trim(strtolower($linktext[$key-1]))
                   and $linkdestination[$key] == $linkdestination[$key-1]
                  ) { 
                    if(!in_array(trim(strtolower($value)),$excludedlinks)){ 
                    $atagcode = $linkcode[$key-1].' ... '.$linkcode[$key];
                    $matchfound = 1;
                }
                }
               
                
            }
            if($matchfound == 1){
	
			// save error
			if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"inconsitent_link_text", $atagcode)){
			$insertid = wp_ada_compliance_basic_insert_error($postinfo,"inconsitent_link_text", $wp_ada_compliance_basic_def['inconsitent_link_text']['StoredError'],  $atagcode);
			}
			

            }

      $count++;
    
}
return 1;
} 
?>