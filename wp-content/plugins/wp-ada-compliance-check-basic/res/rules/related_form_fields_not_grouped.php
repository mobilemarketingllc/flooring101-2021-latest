<?php 
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
/********************************************************************/	
// validate form controls groups without fieldsets 
/********************************************************************/	
function wp_ada_compliance_basic_validate_related_form_fields_not_grouped($content, $postinfo){
	
global $wp_ada_compliance_basic_def;
	
$dom = str_get_html($content);		

// get options
$wp_ada_compliance_basic_scanoptions = get_option('wp_ada_compliance_basic_ignore_scan_rules',array());
	
// check if being scanned
if(in_array('related_form_fields_not_grouped', $wp_ada_compliance_basic_scanoptions)) return 1;	


$forms = $dom->find('form');
foreach ($forms as $form){  
   
$formfields = $form->find('input[type=radio], input[type=checkbox]');
foreach ($formfields as $formfield){
    $fieldnames[] = preg_replace('/\[\d\]/','',$formfield->getAttribute('name'));
}
foreach ($formfields as $formfield){
    $errorfound = 0;
    $parent = $formfield->parent();
    if(count(array_keys($fieldnames, preg_replace('/\[\d\]/','',$formfield->getAttribute('name')))) > 1){
       
         // check for fieldset
        if(isset($parent->tag) and $parent->tag == 'fieldset') $fieldset = $parent;  
        elseif(isset($parent->parent()->tag) and $parent->parent()->tag == 'fieldset') $fieldset = $parent->parent();
        elseif(!is_null($parent->parent()) and isset($parent->parent()->parent()->tag) and $parent->parent()->parent()->tag == 'fieldset') $fieldset = $parent->parent()->parent();
        elseif(!is_null($parent->parent()->parent()) and isset($parent->parent()->parent()->parent()->tag) and $parent->parent()->parent()->parent()->tag == 'fieldset') $fieldset = $parent->parent()->parent()->parent();
        elseif(!is_null($parent->parent()->parent()) and isset($parent->parent()->parent()->parent()->parent()->tag) and $parent->parent()->parent()->parent()->parent()->tag == 'fieldset') $fieldset = $parent->parent()->parent()->parent()->parent();
        elseif(!is_null($parent->parent()->parent()) and isset($parent->parent()->parent()->parent()->parent()->parent()->tag) and $parent->parent()->parent()->parent()->parent()->parent()->tag == 'fieldset') $fieldset = $parent->parent()->parent()->parent()->parent()->parent();
        
        if(!isset($fieldset->tag) or $fieldset->tag != 'fieldset') $errorfound = 1;
        
        if($errorfound == 0){
        $legends = $fieldset->find('legend');  
        if(count($legends) < 1) {
            $errorfound = 1;
        }
        }
        
        // check for div with role = radiogroup or group
        if(isset($parent->tag) and $parent->tag == 'div' 
           and ($parent->getAttribute('role') == 'group' or $parent->getAttribute('role') == 'radiogroup' )
           and $parent->hasAttribute('aria-labelledby'))   $errorfound = 0;  

        
        if($errorfound == 0){
        $legends = $fieldset->find('legend');  
        if(count($legends) < 1) {
            $errorfound = 1;
        }
    }
    
	
    if ($errorfound == 1){
       $errorcode = '';  
    $affectedfields = $form->find('input[name^='.preg_replace('/\[\d\]/','',$formfield->getAttribute('name')).']');
      foreach ($affectedfields as $code){  
    $affectedlabels = $form->find('label[for='.$code->getAttribute('id').']'); 
     foreach ($affectedlabels as $label){
           $errorcode .= $label->outertext;
      }
        $errorcode .= $code->outertext;
    }

			
			// save error
			if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"related_form_fields_not_grouped", $errorcode))
			$insertid = wp_ada_compliance_basic_insert_error($postinfo,"related_form_fields_not_grouped", $wp_ada_compliance_basic_def['related_form_fields_not_grouped']['StoredError'], $errorcode);
			

			
    }
}
}
}
	return 1;
}
?>