<?php
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
/********************************************************************/	
// validate object tags
/********************************************************************/	
function wp_ada_compliance_basic_validate_object_alt_missing($content, $postinfo){
	
global $wp_ada_compliance_basic_def;
	
$dom = str_get_html($content);	

// get options
$wp_ada_compliance_basic_scanoptions = get_option('wp_ada_compliance_basic_ignore_scan_rules',array());
	
// check if being scanned
if(in_array('object_alt_missing', $wp_ada_compliance_basic_scanoptions)) return 1;		

$objecttags = $dom->find('object');
foreach ($objecttags as $object) {
	 if (isset($objecttags) and $object->plaintext == "") {
       	
	 
		 $objectcode = htmlspecialchars($object->outertext);
			
			// save error
			if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"object_alt_missing", $objectcode)){
				
			$insertid = wp_ada_compliance_basic_insert_error($postinfo,"object_alt_missing", $wp_ada_compliance_basic_def['object_alt_missing']['StoredError'], $objectcode);
			}
			
		
		}
	
}
	return 1;
} 
?>