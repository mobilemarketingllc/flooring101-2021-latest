<?php
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
/****************************************
media library image missing alt text
****************************************/
function wp_ada_compliance_basic_validate_img_missing_alt_media($content, $postinfo){
	
global $wp_ada_compliance_basic_def;

// get options
$wp_ada_compliance_basic_scanoptions = get_option('wp_ada_compliance_basic_ignore_scan_rules',array());
	
// check if being scanned
if(in_array('img_missing_alt_media', $wp_ada_compliance_basic_scanoptions)) return 1;
	
	$alt_text = get_post_meta($postinfo['postid'], '_wp_attachment_image_alt', true);
	$imagecode = '<img src="'.esc_url(site_url()).'/wp-content/uploads/'.strip_tags($content).'" width="150" height="75" alt="" />';
	if($alt_text == "") {
		if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"img_missing_alt_media", $imagecode))
			$insertid = wp_ada_compliance_basic_insert_error($postinfo,"img_missing_alt_media", $wp_ada_compliance_basic_def['img_missing_alt_media']['StoredError'], $imagecode);
			

			// display error
			if(!wp_ada_compliance_basic_ignore_check("img_missing_alt_media", $postinfo['postid'], $imagecode, $postinfo['type'])) {
			$_SESSION['my_ada_notices'] .= '<p>';
			$_SESSION['my_ada_notices'] .= $wp_ada_compliance_basic_def['img_missing_alt_media']['DisplayError'];
			if($wp_ada_compliance_basic_def['img_missing_alt_media']['Reference'] != "") $_SESSION['my_ada_notices'] .= ' <a href="'.$wp_ada_compliance_basic_def['img_missing_alt_media']['ReferenceURL'].'" target="_blank" class="adaNewWindowInfo">'.$wp_ada_compliance_basic_def['img_missing_alt_media']['Reference'].' <i class="fa fa-external-link" aria-hidden="true"><span class="wp_ada_hidden">'.__('opens in a new window', 'wp-ada-compliance-basic').'</span></i></a>';  
				$_SESSION['my_ada_notices'] .= $postinfo['ada_complianceview_error_link'];
			$_SESSION['my_ada_notices'] .= '</p>';
			
			}
	}
	}
?>