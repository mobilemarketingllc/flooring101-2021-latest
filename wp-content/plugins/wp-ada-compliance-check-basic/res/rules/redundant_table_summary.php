<?php
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
/********************************************************************/	
// validate tables with summary attributes that duplicate caption
/********************************************************************/	
function wp_ada_compliance_basic_validate_redundant_table_summary($content, $postinfo){
	
global $wp_ada_compliance_basic_def;
	
$dom = str_get_html($content);		

// get options
$wp_ada_compliance_basic_scanoptions = get_option('wp_ada_compliance_basic_ignore_scan_rules',array());
	
// check if being scanned
if(in_array('redundant_table_summary', $wp_ada_compliance_basic_scanoptions)) return 1;	

$tables = $dom->find('table');

foreach ($tables as $table) {	

$tablecode = $table->outertext;	

// get summary text
$summary = strtolower(trim($table->getAttribute('summary')));	

// get caption text
$captiontext = "";		
$captions = $dom->find('caption');
foreach ($captions as $caption) {
$captiontext = strtolower(trim($caption->plaintext));
}
	
if($table != "" and $captiontext != "" and	$captiontext == $summary ){	

		
	// save error
	if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"redundant_table_summary", $tablecode)){		
	$insertid = wp_ada_compliance_basic_insert_error($postinfo,"redundant_table_summary", $wp_ada_compliance_basic_def['redundant_table_summary']['StoredError'],  $tablecode);	
	}
		
	}
	
}
return 1;
}

?>