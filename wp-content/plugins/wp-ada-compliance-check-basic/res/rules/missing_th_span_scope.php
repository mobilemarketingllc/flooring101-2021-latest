<?php
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
/********************************************************************/	
// // validate tables with header cells spanning multiple rows or columns for correct scope
/********************************************************************/	
function wp_ada_compliance_basic_validate_missing_th_span_scope($content, $postinfo){
	
// look at th with colspan attribute > 1
// if found needs to have scope= colgroup	
	
// look for th with rowspan > 1
// if found needs to have scope = rowgroup
	
global $wp_ada_compliance_basic_def;
	
$dom = str_get_html($content);

// get options
$wp_ada_compliance_basic_scanoptions = get_option('wp_ada_compliance_basic_ignore_scan_rules',array());
	
// check if being scanned
if(in_array('missing_th_span_scope', $wp_ada_compliance_basic_scanoptions)) return 1;	

$tables = $dom->find('table');

foreach ($tables as $table) {	

$tablecode = $table->outertext;

$headercells = $table->find('th');

foreach ($headercells as $th) {
	if (isset($th) and (($th->getAttribute('colspan') > 1 and $th->getAttribute('scope') != "colgroup" and $th->getAttribute('scope') != "row" and $th->getAttribute('id') == "") or ($th->getAttribute('rowspan') > 1 and $th->getAttribute('scope') != "rowgroup" and $th->getAttribute('id') == ""))) {
					
	
			// save error
			if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"missing_th_span_scope", $tablecode))
			$insertid = wp_ada_compliance_basic_insert_error($postinfo,"missing_th_span_scope",$wp_ada_compliance_basic_def['missing_th_span_scope']['StoredError'], $tablecode);	
			
			
		}
	
	}
			
}
return 1;
}

?>