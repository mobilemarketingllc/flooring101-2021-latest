<?php 
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
/**********************************************
// check if error exists
**********************************************/
function wp_ada_compliance_basic_error_check($postinfo, $errortype, $object) {
	global $wpdb;
	
		$scantype = sanitize_text_field($postinfo['scantype']);
	$postid = (int)$postinfo['postid'];
	$title = sanitize_text_field($postinfo['title']);
	$type = sanitize_text_field($postinfo['type']);
        $errortype = sanitize_text_field($errortype);
	
	//set flag for onsave scans
	if($scantype == 'onsave') $onsave = 1;
	else $onsave = 0;
	
	$ignre = wp_ada_compliance_basic_get_ignore_value($errortype, $onsave, $type, $object);
		
	$results = $wpdb->get_results( $wpdb->prepare( 'SELECT postid, ignre FROM '.$wpdb->prefix.'wp_ada_compliance_basic where type = %s and postid = %d and errorcode = %s and object = %s ', $type, $postid, $errortype, $object), ARRAY_A );	
	

	if ( $results ) {
	foreach ( $results as $row) {
		
	// if being ignored, don't overwrite value
	if($row['ignre'] == 1)  $ignre = 1;	
		
		// set recordcheck to 1
	$wpdb->query( $wpdb->prepare( 'UPDATE '.$wpdb->prefix.'wp_ada_compliance_basic SET recordcheck = %d, onsave = %d, ignre = %d  WHERE postid = %d and errorcode = %s and object = %s and type = %s', 1, $onsave, $ignre, $postid, $errortype, $object, $type) );	
	
	return 1;
	}
	}
}
/**********************************************
// check if post has errors 
**********************************************/
function wp_ada_compliance_basic_reported_errors_check($postid, $type, $onsave) {
	global $wpdb;

	$query = 'SELECT postid FROM '.$wpdb->prefix.'wp_ada_compliance_basic where type = %s and postid = %d and onsave = %d and ignre != 1'; 
	
	
	$results = $wpdb->get_results( $wpdb->prepare( $query, $type, $postid, $onsave ), ARRAY_A );

if($wpdb->num_rows > 0) return 1;

	return 0;
}

/**********************************************
// insert error
**********************************************/
function wp_ada_compliance_basic_insert_error($postinfo, $errortype, $error, $object ) {
	global $wpdb;
	$ignre = 0;
	
    //$trustedtags = '<p>,<br />,<a>,<img>,<h1>,<h2>,<h3>,<h4>,<h5>,<h6>,<input>,<map>,<area>,<audio>,<video>,<pre>,<textarea>,<label>,<select>,<table>,<tr>,<td>,<th>,<tbody>,<thead>,<span>,<embed>,<object>,<iframe>,<blink>,<noembed>,<i>,<fieldset>,<caption>,<form>,<title>,<legend>';
    //$object = strip_tags($object,$trustedtags);
    $errortype = sanitize_text_field($errortype);
    $error = sanitize_text_field($error);
		$scantype = sanitize_text_field($postinfo['scantype']);
	$postid = (int)$postinfo['postid'];
	$title = sanitize_text_field($postinfo['title']);
	$type = sanitize_text_field($postinfo['type']);
	if(isset($postinfo['taxonomy'])) $taxonomy = sanitize_text_field($postinfo['taxonomy']);
	else $taxonomy = "";
	if(isset($postinfo['externalsrc'])) $externalsrc = sanitize_text_field($postinfo['externalsrc']);
	else $externalsrc = "";
	
	if(isset($postinfo['examplecode'])) $examplecode = strip_tags($postinfo['examplecode'],'<div><img>');
	else $examplecode = "";	
	
	//set flag for onsave scans
	if($scantype == 'onsave') $onsave = 1;
	else $onsave = 0;
	
		$ignre = wp_ada_compliance_basic_get_ignore_value($errortype, $onsave, $type, $object);
	
	$timezone = get_option('timezone_string');
	if($timezone == "") $timezone = 'America/Chicago';
	date_default_timezone_set($timezone);
	$errordate = date('Y-m-d H:i.s', time()+120);
	$userid = get_current_user_id();
	if($userid == "") $userid = 'autoscan';
	
	
	$wpdb->query( $wpdb->prepare( 'INSERT INTO '.$wpdb->prefix.'wp_ada_compliance_basic (postid, object, errorcode, posttitle, type, taxonomy, date, activeuser, ignre, recordcheck, scantype, onsave, externalsrc, examplecode) values(%d,  %s, %s, %s, %s, %s, %s, %s, %d, %d, %s, %s, %s, %s)', $postid, $object, $errortype, $title, $type, $taxonomy, $errordate, $userid, $ignre, 1, $scantype, $onsave, $externalsrc, $examplecode) );
	return $wpdb->insert_id;
}

/**********************************************
// set ignore value
**********************************************/
function wp_ada_compliance_basic_get_ignore_value($errortype, $onsave, $type, $object) {
$report_filtered_errors = 'scanonly';
$strip_redundant_alt_txt = 'true';
$changefontsize ='true';	
$changetext = 'left';
$striptags = 'true';
$striptarget = 'true';
$strip_autoplay = 'true';
$presentation_tables = 'true';
$remove_invalid_img_alt_text = 'true';
$remove_images_linked_to_self = 'true';
$edit_embed_code = 'true';
//$convert_link_titles = 'true';
$correct_ambiguous_link_text = 'true';
	$correct_empty_anchor_tags = 'true';
$wp_ada_compliance_correct_links = 'true';	
$link_email_addresses = 'true'; 
    $wp_ada_compliance_correct_event_handlers = 'true';
    $wp_ada_compliance_correctlinks= 'true';
    $wp_ada_compliance_correctinpagelinks = 'true';
    $wp_ada_compliance_correct_visual_focus = 'true';
        $correct_elementor_icon_and_img_widgets = 'true'; 
    $correct_elementor_TOC = 'true'; 

$ignre = 0;	
	
if($onsave != '0' and $type != 'theme'){		
	if($remove_invalid_img_alt_text == 'true' 
	   	and $errortype == 'img_alt_invalid' 
	   	and (stristr($object, __('graphic of','wp-ada-compliance-basic'))
	  	or stristr($object, __('image of','wp-ada-compliance-basic'))
	  	or stristr($object, __('photo of','wp-ada-compliance-basic'))))	$ignre = 2;	
	if($presentation_tables == 'true' and $errortype == 'missing_th' and stristr($object, 'role-presentation'))	$ignre = 2;
	if($strip_redundant_alt_txt == 'true' and $errortype == 'redundant_title_tag') $ignre = 2; 
	if($strip_redundant_alt_txt == 'true' and $errortype == 'redundant_alt_text') $ignre = 2; 
	if($changefontsize != 'false'  and $errortype == 'absolute_fontsize') $ignre = 2;
	if($changetext != 'false'  and $errortype == 'text_justified') $ignre = 2; 	
	if($striptags == 'true' and $errortype == 'empty_heading_tag') $ignre = 2; 	
	if($striptags == 'true' and $errortype == 'empty_anchor_tag' and !stristr($object, '<img') and !stristr($object, 'fa-') and !stristr($object, '<input') and !stristr($object, '<button')) $ignre = 2; 
    if($correct_elementor_icon_and_img_widgets == 'true' and $errortype == 'empty_anchor_tag' and stristr($object, 'elementor-image-or-icon-box')) $ignre = 2; 
    if($correct_elementor_icon_and_img_widgets == 'true' and $errortype == 'adjacent_identical_links' and stristr($object, 'elementor-image-or-icon-box')) $ignre = 2; 
   // if($correct_elementor_icon_and_img_widgets == 'true' and $errortype == 'missing_alt_text_anchor' and stristr($object, 'elementor-image-or-icon-box')) $ignre = 2; 
	//if($convert_link_titles == 'true' and $errortype == 'empty_anchor_tag' and (stristr($object, 'title') and !stristr($object, 'title=""') and !stristr($object, "title=''"))) $ignre = 2; 
	if($striptarget != 'false' and $errortype == 'new_window_tag') $ignre = 2; 	
	if($striptarget == 'remove' and stristr($object,"window.open") and $errortype == 'new_window_tag')$ignre = 0;
	if($strip_autoplay == 'true' and $errortype == 'av_tag_with_autoplay') $ignre = 2; 	
	if($remove_images_linked_to_self == 'true' and $errortype == 'img_linked_to_self') $ignre = 2;
		if($errortype == 'iframe_missing_title' and $edit_embed_code == 'true' and wp_ada_compliance_basic_check_iframe_for_filtered_src_url('', $object)) $ignre = 2;    
	if($errortype == 'emulating_links' and $wp_ada_compliance_correct_links == 'true') $ignre = 2;
	if($errortype == 'visual_focus_removed') $ignre = 2;
    if($errortype == 'missing_onkeypress' and $wp_ada_compliance_correct_event_handlers == 'true') $ignre = 2;
	if($errortype == 'unlinked_anchors' and $link_email_addresses == 'true' and preg_match("/<a.*?<\/a>(*SKIP)(*F)|[\w-]+@([\w-]+\.)+[\w-]+/i",$object)) $ignre = 2;
    
    if($errortype == 'link_to_non_html_content' and $wp_ada_compliance_correctlinks == 'true') $ignre = 2;
        if($errortype == 'link_to_in_page_content' and $wp_ada_compliance_correctinpagelinks == 'true') $ignre = 2;
     if($errortype == 'visual_focus_removed' and $wp_ada_compliance_correct_visual_focus == 'true' and strstr($object, 'style=')) $ignre = 2;
    if($errortype == 'elementor_toc' and $correct_elementor_TOC == 'true') $ignre = 2;
	
 //if($errortype == 'ambiguous_anchor_tag' and $correct_ambiguous_link_text == 'true' and wp_ada_compliance_basic_check_if_ambiguous_link_can_be_corrected($object)) $ignre = 2;	
}

	return $ignre;
}
/**********************************************
// ignore error
**********************************************/
function wp_ada_compliance_basic_jquery_ignore_error( $id, $direction) {
	global $wpdb;
	
	if ( !current_user_can( "edit_pages" ) ) return 1;
	

	$wpdb->query( $wpdb->prepare( 'UPDATE '.$wpdb->prefix.'wp_ada_compliance_basic set ignre = %d where id  = %d ', $direction, $id) );	
	
	
}
function wp_ada_compliance_basic_ignore_error( $id ) {
	global $wpdb;

	if ( !current_user_can( "edit_pages" ) ) return 1;
	
	// cancel ignore
	if (isset( $_GET[ 'canxignore' ] ) ) {
		$wpdb->query( $wpdb->prepare( 'UPDATE '.$wpdb->prefix.'wp_ada_compliance_basic set ignre = %d where id  = %d ', 0, $id) );
		$_SESSION['my_ada_important_notices'] = __('The selected error is no longer being ignored.','wp-ada-compliance-basic');
	} 
		// ignore just this id
	else {
	$wpdb->query( $wpdb->prepare( 'UPDATE '.$wpdb->prefix.'wp_ada_compliance_basic set ignre = %d where id  = %d ', 1, $id) );

	$_SESSION['my_ada_important_notices'] = __('The selected error is now being ignored.','wp-ada-compliance-basic');
		}
}
/**********************************************
// check ignore status
**********************************************/
function wp_ada_compliance_basic_ignore_check( $errortype, $postid, $object, $type ) {
	global $wpdb;
	$results = $wpdb->get_results( $wpdb->prepare( 'SELECT * FROM '.$wpdb->prefix.'wp_ada_compliance_basic where type= %s and postid = %d and errorcode = %s and object = %s and ignre = %d', $type, $postid, $errortype, $object, 1 ), ARRAY_A );

	if ( $results ) {
		return 1;
	}
}

/**********************************************
// check if font awesome link can be corrected
**********************************************/
function wp_ada_compliance_basic_check_if_font_awesome_link_can_be_corrected($content){

// correct issues with encoding when website is using non utf-8
if(function_exists('mb_convert_encoding'))
$content = mb_convert_encoding($content, 'HTML-ENTITIES', "UTF-8");	

$dom = new DOMDocument;
libxml_use_internal_errors(true);
$dom->loadHTML($content);	

$links = $dom->getElementsByTagName('a');
	
foreach ($links as $link) {
	
$elements = $link->getElementsByTagName("*");
foreach ($elements as $i) {
if(isset($i) and strip_tags(trim($link->nodeValue)) == ''){	
if(stristr($i->getAttribute('class'),'fa-')) return 1;
}
}
}
	
return 0;
}


/**********************************************************
retrieve aria value and return
**********************************************************/
function wp_ada_compliance_basic_get_aria_values($dom, $element, $field){
    
if($field == 'aria-labelledby' and $element->getAttribute('aria-labelledby') != "") {
			$ariaid = $element->getAttribute('aria-labelledby');
    if(isset($dom->getElementById($ariaid)->plaintext))       
    return $dom->getElementById($ariaid)->plaintext;
		}

if($field == 'aria-describedby' and $element->getAttribute('aria-describedby') != "") {
			$ariaid = $element->getAttribute('aria-describedby');
    if(isset($dom->getElementById($ariaid)->plaintext))       
    return $dom->getElementById($ariaid)->plaintext;
} 
    
return ;    
}

/**********************************************
// get error list for affected post
**********************************************/
function wp_ada_compliance_basic_get_error_list_for_post($postid, $posttype){
	global $wpdb, $wp_ada_compliance_basic_def;
    $errornotices = '';
    
$results = $wpdb->get_results( $wpdb->prepare( 'SELECT distinct errorcode FROM '.$wpdb->prefix.'wp_ada_compliance_basic where postid = %d and type = %s and scantype = %s and ignre != %d', $postid, $posttype, 'onsave', '1'), ARRAY_A );

	foreach ( $results as $row ) {
        $errorcode = $row['errorcode'];
			$errornotices .= '<p>';
			$errornotices .= $wp_ada_compliance_basic_def[$errorcode]['DisplayError'];
			if($wp_ada_compliance_basic_def[$errorcode]['Reference'] != "") 
            $errornotices .= ' <a href="'.$wp_ada_compliance_basic_def[$errorcode]['ReferenceURL'].'" target="_blank" class="adaNewWindowInfo">'.$wp_ada_compliance_basic_def[$errorcode]['Reference'].'<i class="fas fa-external-link-alt" aria-hidden="true"><span class="wp_ada_hidden">'.__('opens in a new window', 'wp-ada-compliance-basic').'</span></i></a>';  
			 $errornotices .= '<a href="'.esc_url(get_site_url()).'/wp-admin/admin.php?page=ada_compliance/compliancereportbasic.php&view=1&errorid='.esc_attr($postid).'&type='.esc_attr($posttype).'&iframe=1&TB_iframe=true&width=900&height=550" class="thickbox adaNewWindowInfo adaErrorText adareportlink" target="_blank"><i class="fas fa-eye" aria-hidden="true"></i>';
             $errornotices .= __('View Accessibility Report for Help Options','wp-ada-compliance-basic');
             $errornotices .= '</a>'; 
			$errornotices .= '</p>';
	}
    return $errornotices;
}
/**********************************************
// check if ambiguous link text can be corrected
**********************************************
function wp_ada_compliance_basic_check_if_ambiguous_link_can_be_corrected($content){

// correct issues with encoding when website is using non utf-8
if(function_exists('mb_convert_encoding'))
$content = mb_convert_encoding($content, 'HTML-ENTITIES', "UTF-8");	

$dom = new DOMDocument;
libxml_use_internal_errors(true);
$dom->loadHTML($content);	

// redundant title text
$links = $dom->getElementsByTagName('a');
foreach ($links as $link) {	

if(strstr($link->getAttribute('class'),'flex-prev') or strstr($link->getAttribute('class'),'flex-next')) return 1;	
	
$url = esc_url($link->getAttribute('href'));
$post_id = url_to_postid($url);
if(get_the_title($post_id) != "" and $post_id != 0)return 1;
	
}
	return 0;
}*/
?>